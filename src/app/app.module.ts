import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import {GameModule} from '../game/module'
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LeaderboardModule } from '../leaderboard/module';
import {HomeComponent} from '../home/home'
import { GameService } from '../game/service';
@NgModule({
  declarations: [
    MyApp,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    GameModule,
    LeaderboardModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomeComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GameService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
