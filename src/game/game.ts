import { Component,Input, Output } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GameService } from './service'
import { Question } from './models';
import { Player } from '../leaderboard/models'
import { LeaderBoard } from '../leaderboard/component'
import { QuestionComponent} from '../game/question/component'
@Component({
  selector: 'page-game',
  templateUrl: 'game.html'
})
export class GamePage {
  @Output('answer') answer : String

  public questions: Question[] =[];
  public question: Question
  public nickname: String = "Player1"
  public level: String = ""
  public avatar: String = ""
  public player :Player
  public score: Number = 0
  public timer: Number = 0
  public difficulty: String
  public correctAnswers: String
  
  constructor(public navController: NavController, public navParams: NavParams,
    public gameService: GameService,public questionComponent :QuestionComponent) {
    this.nickname = navParams.get('nickname');
    this.level = navParams.get('level');
  }

  ionViewWillEnter() {
    switch(this.level){
      case 'easy' : this.gameService.loadQuestionsEasy();
      break;
      case 'medium' : this.gameService.loadQuestionsMedium();
      break;
      case 'hard' :this.gameService.loadQuestionsHard();
      break;
      default:this.gameService.loadQuestionsEasy();
    }
    this.gameService.subject.asObservable()
      .subscribe(questions => {
        this.questions = questions
      })

  }
  isCorrectAnswers() {
    this.questions.forEach((question:Question)=> {
        if(question.correct_answer == question.userAnswer){
        switch (question.difficulty) {
          case 'easy': this.score = +5
          break;
          case 'medium': this.score = +10
          break;
          case 'hard': this.score = +15
          break;
      }
    }
    else {
      switch (question.difficulty) {
          case 'easy': this.score = - 5
          break;
          case 'medium': this.score = -10
          break;
          case 'hard': this.score = -15
          break;
      }
  }
      });

}
  endGame() {
    this.isCorrectAnswers()
    this.player = {
      nickname: this.nickname,
      score: this.score,
      time: this.timer,
      avatar_url: this.avatar
    }
    this.gameService.postLeaderBoard(this.player)
    this.navController.push(LeaderBoard)
  }
}