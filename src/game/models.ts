
interface Question {
    question : String ,
    category: String,
    type: String,
    difficulty:String,
    correct_answer:String,
    incorrect_answers:Array<String>,
    userAnswer:String 
}
export {Question}