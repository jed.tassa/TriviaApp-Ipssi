import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { IonicModule } from "ionic-angular";
import { GameService } from "./service";
import { SharedModule } from "../shared/module";
import { GamePage } from "./game";
import { QuestionComponent } from "./question/component";

@NgModule({
    imports: [
        BrowserModule,
        IonicModule,
        SharedModule
],
    entryComponents: [GamePage],
    declarations: [ GamePage,QuestionComponent],
    providers: [ GameService,QuestionComponent ],
    exports: [ GamePage ]
})
export class GameModule { }
