import { Component,Input,OnInit } from "@angular/core";
import { Player } from "../../leaderboard/models";

@Component({
    selector:'player-component',
    templateUrl:'template.html',
})

export class PlayerComponent implements OnInit {
    @Input('player') player: Player

    ngOnInit(){
    }
}