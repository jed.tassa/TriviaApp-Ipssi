import { Component, Input, OnInit } from "@angular/core";
import { NavParams, NavController } from 'ionic-angular';
import { Question } from "../models";
import { Player } from "../../leaderboard/models"
import { GameService } from "../service";

@Component({
    selector: 'question-component',
    templateUrl: 'template.html',
})

export class QuestionComponent implements OnInit {
    @Input('question') question: Question
    @Input('index') index:Number
    
    public answerUser:String = ""
    public player: Player
    public timer : Number = 0

    
    constructor(public gameService:GameService){}
    
    ngOnInit() 
    {
      
    }
}