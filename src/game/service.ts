import { Injectable } from "@angular/core";
import { HttpService } from "../shared/http.service";
import { Question } from "./models";
import { Subject } from "rxjs/Subject";
import { Player } from '../leaderboard/models'
@Injectable()
export class GameService {
    public username : String
    subject: Subject<Array<Question>> = new Subject();
    constructor(public httpService: HttpService) {}
    loadQuestionsEasy() {
        const numberQuestions = 20
        
        return new Promise(((resolve, reject) => {
            this.httpService.getGame(`?amount=${numberQuestions}&difficulty=easy`)
                .subscribe(
                    response => {
                        //DecodeURIComponent
                        this.subject.next(response.json().results)
                        resolve(response.json().results)
                    },
                    error => {
                        this.subject.error(error.json())
                        reject(error);
                    }
                )
        }))
    }
    loadQuestionsMedium() {
        const numberQuestions = 20
        return new Promise(((resolve, reject) => {
            this.httpService.getGame(`?amount=${numberQuestions}&difficulty=medium`)
                .subscribe(
                    response => {
                        //DecodeURIComponent
                        this.subject.next(response.json().results)
                        resolve(response.json().results)
                    },
                    error => {
                        this.subject.error(error.json())
                        reject(error);
                    }
                )
        }))
    }
    loadQuestionsHard() {
        const numberQuestions = 20
        return new Promise(((resolve, reject) => {
            this.httpService.getGame(`?amount=${numberQuestions}&difficulty=hard`)
                .subscribe(
                    response => {
                        //DecodeURIComponent
                        this.subject.next(response.json().results)
                        resolve(response.json().results)
                    },
                    error => {
                        this.subject.error(error.json())
                        reject(error);
                    }
                )
        }))
    }
    postLeaderBoard(player: Player) {

        return new Promise(((resolve, reject) => {
            this.httpService.postBoard(`api/score`,player)
                .subscribe(
                    response => {
                        this.subject.next(response.json().results)
                        resolve(response.json().results)
                    },
                    error => {
                        this.subject.error(error.json())
                        reject(error);
                    }
                )
        }))
    }
}