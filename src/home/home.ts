import { Component,Input } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular'
import {GamePage} from '../game/game'
@Component({
  selector: 'home',
  templateUrl: 'template.html'
})
export class HomeComponent{
  @Input('level') level : String
  public nickname : String = ""
  constructor(public navController: NavController,
    public navParams: NavParams){}
    startGame(){
      this.navController.push(GamePage, {
        id: "1",
        nickname: this.nickname,
        level: this.level
      });
    }
}
