import { Component, } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LeaderboardService } from './service'
import { Player } from "./models";
@Component({
  selector: 'page-leaderboard',
  templateUrl: 'template.html'
})
export class LeaderBoard {
  public players: Player[];
  public player: Player

  constructor(public navController: NavController, public leaderboardService: LeaderboardService) { }


  ionViewWillEnter() {
    this.leaderboardService.loadLeaderboard();
    this.leaderboardService.subject.asObservable()
      .subscribe(players => this.players = players)
  }


}