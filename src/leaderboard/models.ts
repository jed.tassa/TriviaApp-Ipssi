interface Player {
    nickname : String,
    score : Number,
    time: Number,
    avatar_url: String,
}

export {Player}