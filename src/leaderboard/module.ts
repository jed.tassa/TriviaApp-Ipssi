import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { IonicModule } from "ionic-angular";
import { LeaderboardService } from "./service";
import { SharedModule } from "../shared/module";
import { LeaderBoard } from "./component";
import { PlayerComponent} from "../game/player/component"

@NgModule({
    imports: [
        BrowserModule,
        IonicModule,
        SharedModule
    ],
    entryComponents: [LeaderBoard],
    declarations: [ LeaderBoard,PlayerComponent],
    providers: [ LeaderboardService, ],
    exports: [ LeaderBoard ]
})
export class LeaderboardModule{}