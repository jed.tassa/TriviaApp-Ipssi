import { Injectable } from "@angular/core";
import { HttpService } from "../shared/http.service";
import { Player } from "./models";
import { Subject } from "rxjs/Subject";

@Injectable()
export class LeaderboardService{
    subject: Subject<Array<Player>> = new Subject();
    constructor(public httpService: HttpService) {}
    loadLeaderboard() {
        return new Promise(((resolve,reject) => {
            this.httpService.getBoard(`api/json`)
            .subscribe(
                response =>{
                    this.subject.next(response.json())
                    resolve(response.json())
                },
                error => {
                    this.subject.error(error.json())
                        reject(error);
                }
            )
        }))
    }
    
}
