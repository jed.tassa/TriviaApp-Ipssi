import { Injectable } from "@angular/core";

@Injectable()

export class Config {
    LEADERBOARD_URL: string = "https://leaderboard.lp1.eu/"
    QUESTIONS_URL:string = "https://opentdb.com/api.php"
}