import { Injectable } from "@angular/core";
import { Http, RequestOptions } from '@angular/http';
import { Headers } from "@angular/http";
import { Config } from "./config";

@Injectable()
export class HttpService {
    constructor(public http: Http, public config: Config) {}

    getHeaders() {
        const token = localStorage.getItem('token');
        if (token) {
            return new Headers({
                Authorization: token,
            })
        }
        return new Headers({});
    }
    getBoard(resource) {
        const options = new RequestOptions()
        options.headers = this.getHeaders()
        return this.http.get(this.config.LEADERBOARD_URL + resource, options);
    }
    postBoard(resource, data) {
        const options = new RequestOptions()
        options.headers = this.getHeaders()
        return this.http.post(this.config.LEADERBOARD_URL + resource, data, options);
    }
    getGame(resource) {
        const options = new RequestOptions()
        options.headers = this.getHeaders()
        return this.http.get(this.config.QUESTIONS_URL + resource, options);
    }
}
