import { NgModule } from "@angular/core";
import { HttpModule } from "@angular/http";
import { Config } from "./config";
import { HttpService } from "./http.service";

@NgModule({
    providers: [ Config, HttpService ],
    imports: [ HttpModule ]
})

export class SharedModule{}